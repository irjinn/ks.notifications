require('./serialize-errors')();
const stringUtils = require('./string-utils');

String.prototype.replaceAll = function (oldValue, newValue) {
  return stringUtils.replaceAll(this, oldValue, newValue);
};
