// todo: write as function, accept parameters
module.exports = function(options){
  const alreadyRegistered = 'toJSON' in Error.prototype;
  if(alreadyRegistered){
    return;
  }
  options = options || {};
  const verbose = options.verbose === true;
  const diagnosticsProps = ['stack'];
  
  Object.defineProperty(Error.prototype, 'toJSON', {
    value: function () {
      var alt = {};
  
      Object.getOwnPropertyNames(this).forEach(function (key) {
        if(verbose || diagnosticsProps.indexOf(key) < 0){
          alt[key] = this[key];
        }
      }, this);
  
      return alt;
    },
    configurable: true,
    writable: true
  });
}