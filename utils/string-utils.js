var stringUtils = module.exports = {};

stringUtils.escapeRegexString = function(str) {
  return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
};
stringUtils.replaceAll = function(sourceString, oldToken, newToken){
  var escapedToken = stringUtils.escapeRegexString(oldToken);
  var regex = new RegExp(`${escapedToken}`, 'g');
  return sourceString.replace(regex, newToken);
};
