const config = require('../config');
const DocumentDbStorage = require('../documentdb').storage;
const storage = new DocumentDbStorage(config.documentdb);
const log = require('../logger');

if(config.documentdb == null || config.documentdb.collections == null || config.documentdb.collections.devices == null){
  throw new Error('documentDb configuration is missing');
}

const collectionPath = config.documentdb.collections.devices.split('/');
if(collectionPath.length !== 2){
  throw new Error('configuration incorrect: documentdb.collections.devices, exprected: db/coll')
}
/**
 * @typedef DevicesFilter
 * @property {boolean} raw If set to true, returns raw documentDb objects.
 * @property {boolean} all If set to true, returns all devices (without AllowNotification filtering).
 * @property {number} take Take limit. Default is 100.
 * @property {string} userEmail Filter by single user email.
 * @property {string} userName Filter by single user login.
 * @property {string[]} emails Filter by array of user emails.
 */

const deviceMapping = `
x.userId as userId, 
x.userName as userName, 
x.userEmail as userEmail, 
x.type as type, 
x.deviceToken as deviceToken,
x.model as model,
x.os as os,
x.allowNotifications as allowNotifications,
x.id as id
`.replace(/(?:\r\n|\r|\n)/g, '');

// todo: sanitize input (only for internal use, so not critical at all) 
const sql = {
  /**
   * Takes devices filter, returns sql query.
   * @param {DevicesFilter} filter Optional parameters.
   * @returns {string} Sql query.
   */
  queryDevices(filter){
    filter = filter || {};
    const projection = !filter.raw ? deviceMapping : '*';
    const take = (filter.take > 0 && filter.take < 1001) ? filter.take : 1000;
    let query = `SELECT TOP ${take} ${projection} FROM items x WHERE x.dbType = 'userdevice'`;
    if(filter.userEmail){
      query += ` AND x.userEmail = '${filter.userEmail}'`;  
    }
    if(!filter.all){
      query += ` AND x.allowNotifications = true`;  
    }
    if(Array.isArray(filter.emails) && filter.emails.length > 0){
      let expression = filter.emails.map(x => `'${x.toLowerCase()}'`).join(',');
      query += ` AND x.userEmail IN (${expression})`;
    }
    if(filter.platform){
      query += ` AND x.type = '${filter.platform}'`;  
    }
    if(filter.userName){
      query += ` AND CONTAINS(x.userName, '${filter.userName}')`;  
    }
    query += ` ORDER BY x._ts DESC`;
    return query;
  }
}

module.exports = {
  /**
   * Fetch user devices.
   * @param {DevicesFilter} filter Optional parameters.
   * @param {number} filter.take Take limit. Default is 100.
   * @returns {Promise<object[]>} Devices.
   */
  async query(filter){
    filter = filter || {};
    filter = {
      raw: filter.raw,
      all: filter.all,
      platform: filter.platform,
      take: filter.take,
      userEmail: filter.userEmail,
      userName: filter.userName,
      emails: filter.emails,
    };
    log.debug({msg: 'fetching devices from db', data: { filter }})
    const db = await storage.getDatabase(collectionPath[0]);
    const col = await db.getCollection(collectionPath[1]);
    const query = sql.queryDevices(filter);
    let result = await col.toArray(query);
    return result;
  }
}

