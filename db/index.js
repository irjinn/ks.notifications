/**
 * DataAccess facade
 */

module.exports = {
  devices: require('./devices'),
  tasks: require('./tasks')
}