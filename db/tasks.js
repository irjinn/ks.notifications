const config = require('../config');
const DocumentDbStorage = require('../documentdb').storage;
const storage = new DocumentDbStorage(config.documentdb);
const log = require('../logger');
// require('../models');

if(config.documentdb == null || config.documentdb.collections == null || config.documentdb.collections.tasks == null){
  throw new Error('documentDb configuration is missing');
}

const collectionPath = config.documentdb.collections.tasks.split('/');
if(collectionPath.length !== 2){
  throw new Error('configuration incorrect: documentdb.collections.devices, exprected: db/coll')
}

const taskDbType = 'task';

/**
 * @typedef TasksFilter
 * @property {boolean} raw If set to true, returns raw documentDb objects.
 * @property {number} take Take limit. Default is 100.
 */

const taskStatus = {
  created: 'Created',
  inProgress: 'InProgress',
  faulted: 'Faulted',
  completed: 'Completed',
}

// date.toISOString() should be ok for:
//  x.Modified
//  x.Created

const taskMapping = `
  x.id as id,
  x.TaskType as type, 
  x.Payload as payload
`.replace(/(?:\r\n|\r|\n)/g, '');

const sql = {
  /**
   * Takes filter, returns sql query.
   * @param {TasksFilter} filter Optional parameters.
   * @returns {string} Sql query.
   */
  queryTasks(filter){
    filter = filter || {};
    const projection = !filter.raw ? taskMapping : '*';
    const take = (filter.take > 0 && filter.take < 1001) ? filter.take : 1000;
    let query = `SELECT TOP ${take} ${projection} FROM items x WHERE x.DbType = '${taskDbType}'`;
    // query += ` ORDER BY x.Modified DESC`;
    return query;
  }
}

module.exports = {
  /**
   * Enum with task statuses as strings.
   */
  status: taskStatus,
  /**
   * Fetch tasks.
   * @param {TasksFilter} filter Optional parameters.
   * @param {number} filter.take Take limit. Default is 100.
   * @returns {Promise<object[]>}
   */
  async query(filter){
    filter = filter || {};
    filter = {
      raw: filter.raw,
      take: filter.take || 100,
    };
    const query = sql.queryTasks(filter);
    log.debug({msg: 'fetching tasks from db', data: { filter, query }});
    const db = await storage.getDatabase(collectionPath[0]);
    const col = await db.getCollection(collectionPath[1]);
    let result = await col.toArray(query);
    return result;
  },

  /**
   * Upsert task.
   * @param {TaskModel} task 
   */
  async upsert(task){
    const db = await storage.getDatabase(collectionPath[0]);
    const col = await db.getCollection(collectionPath[1]);
    return await col.upsert(task);
  }
}
