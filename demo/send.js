require('../utils/extensions'); //serialize errors
const json = require('circular-json');
const sender = require('../services').sender;

let payload = {
  type: 'book',
  thumbnail: "https://prodksbooks.blob.core.windows.net/edition-covers/978-91-88521-03-3.jpg",
  bookId: 'c0e1cba7-821d-4199-a007-15949b1717d3'
}

let notification = {
  title: 'Notification title',
  body: 'This is notification body',
  sound: 'default',
  subtitle: 'subtitle',
  icon: 'ic_launcher',
  custom: payload
};

const iosDeviceToken = 'd061b78da30213ec3d9d1a21540d0fa867caf928af557787530c9adc3684205e';
//
const zteDeviceToken = 'dFZiGWpQ9pw:APA91bGLBLummNCCyFiHaSDwBxwZGf7cbQ4pvIFZq7QL3osbkjuTFB4DNxTZNEB-7WDSVkYQgUPcJa-qLx09UeAXj2TpqGLEeEQD8DN7WUSSxkzAy_KMXe96Fb0iKo4r0GwyBoeDLspT';
const htcDeviceToken = 'daaEgI3Ce_U:APA91bG6PfWQ6Id7nPXUejLLgkzuXZ2jHW3EHPhgL0122fNsYPpfCLd1WVzhqK8CBTjbK94vmY-J_Hoq8p7jd0sbshKYG8jZ7QEInpEcMVru2s3gUUI8KpLKpOtCF9cVzTwUWv1WNJNV';
const xaomi = 'd9IqLqsWfdA:APA91bH24r0F23pEc8TUmkXqyZOaqX_wDrTFnrULageaTDp1R1LwoH1WPgyN2ArcbWT-Kqqlza9VHyWyokEdryX67JjJnY5nVq_zdc7DOM2D4MRgeb2_cjh7H18KWymxKmm9qvhVaMKR';
const ids = [
  xaomi, 
  htcDeviceToken,
  // iosDeviceToken
];

sender.send(ids, notification)
  .then(x => console.log(json.stringify(x, null, 2)));

  