require('../utils/serialize-errors');
const json = require('circular-json');
const DocumentDBClient = require('documentdb').DocumentClient;
const fs = require('fs');
const config = require('../config');
const util = require('util');

const DocumentDbStorage = require('../documentdb/documentdb-storage');
const storage = new DocumentDbStorage(config.documentdb);

async function demo(){
  try{
    let result = await playground();
    // let result = await devices.getAll();

    dump(result);
  }
  catch(err){
    console.log('!error occured');
    dump(err);
  }
}

async function playground(){
  let db = await storage.getDatabase('catalogue');
  let col = await db.getCollection('editions');
  let projection = 'x.DeviceToken as deviceToken';
  // projection = '*';
  let querySql = `SELECT ${projection} FROM x WHERE x.DbType = 'userdevice'`;

  // let iterator = storage.client.queryDocuments(col.link, querySql);
  let iterator = col.find(querySql);
  let toArray = util.promisify(iterator.toArray).bind(iterator);
  
  return {
    res: await toArray(),
  };
}

//todo: devices by userId or userEmail or userName

async function query(){
  let db = await storage.getDatabase('catalogue');
  let col = await db.getCollection('editions');
  // let projection = '{"userId": x.UserId, "userName": x.UserName} as User';
  projection = '*';
  let querySql = `SELECT TOP @take ${projection} FROM x WHERE x.DbType = 'userdevice' ORDER BY x.Modified DESC`;
  let query = {
    query: querySql,
    parameters: [
      {
        name: '@take',
        value: 3
      }
    ]
  }

  let devices = await col.toArray(query);
  return devices;
}

demo();
 
function dumpToFileSync(data, filePath){
  filePath = filePath || 'C:\\Temp\\dev\\output.json';
  var content = JSON.stringify(data, null, 2);
  fs.writeFileSync(filePath, content);
}
function dump(obj){
  dumpToFileSync(obj)
  console.log(json.stringify(obj, null, 2));
  return obj;
}
