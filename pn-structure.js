let note = new apn.Notification();
note.title = 'Kitab';
note.subtitle = 'الطريق الأقل ارتياداً';
note.topic = 'com.kitabsawti.kitabsawti'; //bundle identifier
note.sound = 'default';
note.payload = {bookId: 'c0e1cba7-821d-4199-a007-15949b1717d3'};

// new apn.Notification({})
const message = {
  retryLimit: data.retries || -1,
  expiry: data.expiry || ((data.timeToLive || 28 * 86400) + Math.floor(Date.now() / 1000)),
  priority: data.priority === 'normal' ? 5 : 10,
  encoding: data.encoding,
  payload: data.custom || {},
  badge: data.badge,
  sound: data.sound || 'ping.aiff',
  alert: data.alert || {
      title: data.title,
      body: data.body,
      'title-loc-key': data.titleLocKey,
      'title-loc-args': data.titleLocArgs,
      'loc-key': data.locKey,
      'loc-args': data.bodyLocArgs,
      'launch-image': data.launchImage,
      action: data.action,
  },
  topic: data.topic,
  category: data.category || data.clickAction,
  contentAvailable: data.contentAvailable,
  mdm: data.mdm,
  urlArgs: data.urlArgs,
  truncateAtWordEnd: data.truncateAtWordEnd,
  collapseId: data.collapseKey,
  mutableContent: data.mutableContent || 0,
};

// new gcm.Message({})
const notification = {
    title: data.title, // Android, iOS (Watch)
    body: data.body, // Android, iOS
    icon: data.icon, // Android
    sound: data.sound, // Android, iOS
    badge: data.badge, // iOS
    tag: data.tag, // Android
    color: data.color, // Android
    click_action: data.clickAction || data.category, // Android, iOS
    body_loc_key: data.locKey, // Android, iOS
    body_loc_args: data.locArgs, // Android, iOS
    title_loc_key: data.titleLocKey, // Android, iOS
    title_loc_args: data.titleLocArgs, // Android, iOS
};

let custom;
if (typeof data.custom === 'string') {
    custom = {
        message: data.custom,
    };
} else if (typeof data.custom === 'object') {
    custom = Object.assign({}, data.custom);
} else {
    custom = {
        data: data.custom,
    };
}

custom.title = custom.title || data.title || '';
custom.message = custom.message || data.body || '';
custom.sound = custom.sound || data.sound || undefined;
custom.icon = custom.icon || data.icon || undefined;
custom.msgcnt = custom.msgcnt || data.badge || undefined;
if (opts.phonegap === true && data.contentAvailable) {
    custom['content-available'] = 1;
}

const message = new gcm.Message({ // See https://developers.google.com/cloud-messaging/http-server-ref#table5
    collapseKey: data.collapseKey,
    priority: data.priority === 'normal' ? data.priority : 'high',
    contentAvailable: data.contentAvailable || false,
    delayWhileIdle: data.delayWhileIdle || false,
    timeToLive: data.expiry - Math.floor(Date.now() / 1000) || data.timeToLive || 28 * 86400,
    restrictedPackageName: data.restrictedPackageName,
    dryRun: data.dryRun || false,
    data: opts.phonegap === true ? Object.assign(custom, notification) : custom, // See https://github.com/phonegap/phonegap-plugin-push/blob/master/docs/PAYLOAD.md#android-behaviour
    notification: opts.phonegap === true ? undefined : notification,
});