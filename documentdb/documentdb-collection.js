require('./db-types');
const DocumentDBClient = require('documentdb').DocumentClient;
const util = require('util');

module.exports = class DocumentDbCollection{
  /**
   * @param {DocumentDBClient} client 
   * @param {CollectionMeta} collectionMetadata 
   * @returns {DocumentDbCollection} Instance of DocumentDbCollection.
   */
  constructor(client, collectionMetadata){
    this.client = client;
    this.meta = collectionMetadata;
    this.link = collectionMetadata._self;
  }

  /**
   * Upsert document.
   * @param {object} body The body of the document. Can contain any number of user defined properties.
   * @param {string} body.id The id of the document, MUST be unique for each document.
   * @param {number} body.ttl The time to live in seconds of the document.
   * @param {RequestOptions} [options] The request options.
   * @param {boolean} [options.disableAutomaticIdGeneration] Disables the automatic id generation. If id is missing in the body and this option is true, an error will be returned.
   * @returns {Promise<object>} Updated document.
   */
  upsert(body, options){
    return new Promise((resolve, reject) => {
      this.client.upsertDocument(this.link, body, options, (err, response) => {
        if(err) {
          reject(err);
        }
        else {
          resolve(response);
        }
      })
    });
  }

  /**
   * Query documents and return documentdb iterator.
   * @param query          - A SQL query string.
   * @param [options]      - Represents the feed options.
   * @returns              - An instance of queryIterator to handle reading feed.
   */
  find(query, options){
    return this.client.queryDocuments(this.link, query, options);
  }

  /**
   * Query documents and load them into array.
   * @param {SqlQuerySpec | string} query A SQL query.
   * @param {FeedOptions} [options] Represents the feed options.
   * @param {object} [options.partitionKey] Optional partition key to be used with the partition resolver
   * @returns {Promise<object[]>} Documents array.
   */
  toArray(query, options){
    const iterator = this.client.queryDocuments(this.link, query, options);
    return new Promise((resolve, reject) => {
      iterator.toArray((err, res) => {
        if(err){
          reject(err);
        }
        else{
          resolve(res);
        }
      })
    });
  }
}

/**
 * documentDb query examples: https://www.documentdb.com/sql/demo
 */
