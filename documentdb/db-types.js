/**
 * @typedef DocumentDbCredentials
 * @property {string} endpoing
 * @property {string} key
 */

/**
 * @typedef SqlQuerySpec
 * @property {string} query
 * @property {SqlParameter[]} parameters
 */
/**
 * @typedef SqlParameter
 * @property {string} name
 * @property {object} value
 */
 
/**
 * @typedef FeedOptions
 * @property {Number} maxItemCount Max number of items to be returned in the enumeration operation.
 * @property {string} partitionKey Partition key to be used with the partition resolver.
 * @property {string} continuation Opaque token for continuing the enumeration. 
 * @property {string} sessionToken Token for use with Session consistency.
 * @property {boolean} enableScanInQuery Allow scan on the queries which couldn't be served as indexing was opted out on the requested paths.
 */

/**
 * @typedef AbstractMeta
 * @property {string} id
 * @property {string} _self
 * @property {string} _rid
 * @property {string} _etag
 * @property {string} _attachments 
 */
/**
 * @typedef CollectionMeta
 * @property {string} id
 * @property {string} _self
 * @property {string} _rid
 * @property {string} _etag
 * @property {Number} _ts 
 * @property {string} _attachments 
 * @property {string} _docs 
 * @property {string} _sprocs 
 * @property {string} _triggers 
 * @property {string} _udfs 
 * @property {string} _conflicts 
 */


/**
 * @typedef QueryError
 * @property {string} body
 * @property {Number} code
 */