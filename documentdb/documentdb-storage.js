require('./db-types');
const DocumentDBClient = require('documentdb').DocumentClient;
const DocumentDbDatabase = require('./documentdb-database');

module.exports = class DocumentDbStorage{
  /**
   * @param {DocumentDbCredentials} config 
   */
  constructor(config){
    this.client = new DocumentDBClient(config.endpoint, { masterKey: config.key });

    /**@type {Map<string, DocumentDbDatabase} */
    this._databases = new Map();
  }
  
  /**
   * Find database with given name 
   * @param {string} id
   * @returns {Promise<DocumentDbDatabase|undefined>} database 
   */
  async getDatabase(id){
    let databases = await this._findDatabasesMetadata();
    let dbMeta = databases.find(x => x.id === id);
    if(dbMeta === undefined) return undefined;
    
    let cached = this._databases.get(dbMeta.id);
    if(cached === undefined){
      cached = new DocumentDbDatabase(this.client, dbMeta);
      this._databases.set(dbMeta.id, cached);
    }
    return cached; 
  }

  /**
   * List all databases 
   * @returns {Promise<AbstractMeta[]>} databases
   */
  _findDatabasesMetadata(){
    let querySpec = {query: 'SELECT * FROM root r'};
    return new Promise((resolve, reject) => {
      this.client.queryDatabases(querySpec).toArray((err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    });
  }
}