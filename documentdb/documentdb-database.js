require('./db-types');
const DocumentDBClient = require('documentdb').DocumentClient;
const DocumentDbCollection = require('./documentdb-collection');

module.exports = class DocumentDbDatabase{
  /**
   * @param {DocumentDBClient} client 
   * @param {AbstractMeta} databaseMetadata 
   */
  constructor(client, databaseMetadata){
    this.client = client;
    this.meta = databaseMetadata;
    this.link = databaseMetadata._self;

    /**@type {Map<string, DocumentDbCollection} */
    this._collections = new Map();
  }
  /**
   * 
   * @param {string} id 
   * @returns {DocumentDbCollection} collection
   */
  async getCollection(id){
    let colMeta = await this._findCollectionMetadata(id);
    if(colMeta === undefined) return undefined;
    let cached = this._collections.get(colMeta.id);
    if(cached === undefined){
      cached = new DocumentDbCollection(this.client, colMeta);
      this._collections.set(colMeta.id);
    }
    return cached;
  }
  /**
   * Find collection metadata if exists
   * @param {string} id collection id
   * @returns {Promise<CollectionMeta>} collection metadata
   */
  _findCollectionMetadata(id){
    return new Promise((resolve, reject) => {
      var querySpec = {
        query: 'SELECT * FROM root r WHERE r.id=@id',
        parameters: [
          {
            name: '@id',
            value: id
          }
        ]
      };
      this.client.queryCollections(this.link, querySpec).toArray((err, results) => {
        if (err) {
          reject(err);
        } else if (results.length === 0) {
          return undefined;
        } else {
          resolve(results[0]);
        }
      });
    });
  }
}

