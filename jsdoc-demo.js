/**
 * Represents a person
 * @constructor
 * @param {string} name The name of the person
 */
function Person(name) {
  this.name = name;
}

/**
* Greets the person
* @param {string} greeting The greeting
*/
Person.prototype.greet = function greet(greeting) {
  return greeting + ' ' + this.name;
}
var p = new Person('joe');


/**
 * @typedef Product
 * @property {string} title
 * @property {boolean} price
 */
/**
* @type {Product}
*/
var pr = JSON.parse('{}');
