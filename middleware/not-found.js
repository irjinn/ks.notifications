const fs = require('fs');

module.exports = function pageNotFound(ctx) {
  ctx.status = 404;
  switch (ctx.accepts('html', 'json')) {
    case 'html':
      ctx.type = 'text/html';
      ctx.body = fs.createReadStream('404.html');
      break;
    case 'json':
      ctx.body = {
        message: 'Page Not Found'
      };
      break;
    default:
      ctx.type = 'text';
      ctx.body = '404 Page Not Found';
  }
};