module.exports = {
  auth: require('./auth'),
  logger: require('./request-logger'),
  serverError: require('./server-error'),
  badRequest: require('./bad-request'),
  notFound: require('./not-found'),
  cors: require('./cors'),
};