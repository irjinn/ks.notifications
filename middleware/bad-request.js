module.exports = async function pageNotFound(ctx, next) {
  try {
    await next();
  } catch (err) {
    if (err.status === 400) {
      ctx.status = 400;
      ctx.body = { message: err.message };
    } else {
      throw err;
    }
  }
};
