const config = require('../config');
const log = require('../logger');

module.exports = async function(ctx, next){
  try{
    await next();
    log.trace(contextLogEntry(ctx));
  } 
  catch(err){
    let logEntry = contextLogEntry(ctx);
    logEntry.err = err;
    log.error(logEntry);
    throw err;
  }
};
function contextLogEntry(ctx){
  ctx = ctx || {};
  state = ctx.state || {};
  return {
    auth: state.authenticated, 
    method: ctx.method, 
    url: ctx.url 
  };
}