module.exports = async function allowCors(ctx, next) {
  ctx.response.set('Access-Control-Allow-Origin', '*');
  ctx.response.set('Access-Control-Allow-Headers', 'X-Requested-With');
  ctx.response.set('Access-Control-Allow-Headers', 'Content-Type');
  await next();
};