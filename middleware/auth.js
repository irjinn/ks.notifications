const config = require('../config');

module.exports = async function(ctx, next){
  let authToken = ctx.headers['authorization'] || ctx.query['api-key'] || ctx.query['apikey'];
  let authenticated = authToken === config.api.key;
  ctx.state.authenticated = authenticated;
  ctx.ensureAuthenticated = authenticated 
    ? () => {} 
    : () => { 
      var err = new Error(); 
      err.status = 401; 
      throw err; 
    };

  try {
    await next();
  } catch (err) {
    if (err.status === 401) {
      ctx.status = 401;
      //ctx.set('WWW-Authenticate', 'Basic');
      ctx.body = '{"message": "authentication required"}';
    } else {
      throw err;
    }
  }
}
