module.exports = async function serverError(ctx, next) {
  try {
    await next();
  } catch (err) {
    ctx.status = 500;
    ctx.body = err;
  }
};
