const db = require('../db');

module.exports = {
  /**
   * Fetch user devices.
   * @param {DevicesFilter} filter Optional parameters.
   * @param {number} filter.take Take limit. Default is 100.
   * @returns {Promise<object[]>} Devices.
   */
  query: async function(filter){
    filter.take = (filter.take > 0 && filter.take < 1001) ? filter.take : 10;
    return await db.devices.query(filter);
  },

  /**
   * From array of recipients (deviceTokens or emails)
   * returns array of device tokens.
   * @param {string[]} recipients
   * @param {string} filter.platform ios / android
   * @returns {Promise<string[]>} Device tokens.
   */
  deviceTokens: async function(recipients, filter){
    let result = [];
    let emails = [];
    filter = filter || {};

    for(var key of recipients){
      if(!key){
        continue;
      }
      if(key.indexOf('@') > 0){ // if email --> fetch corresponding devices
        emails.push(key);
        continue;
      }
      result.push(key); // assume it's a device token
    }
    if(emails.length > 0){
      let dbQueryFilter = { emails: emails, platform: filter.platform };
      let devices = await db.devices.query(dbQueryFilter);
      for(let device of devices){
        if(device.deviceToken && typeof device.deviceToken === 'string'){
          result.push(device.deviceToken);
        }
      }
    }
    return result;
  }
}