const config = require('../config');
const errors = require('../errors');
const log = require('../logger');
const db = require('../db');

/**
 * @typedef TaskDto
 * @property {string} id
 * @property {string} type 
 * @property {string} status 
 * @property {object} payload 
 */
/**
 * @typedef TaskModel
 * @property {string} id
 * @property {string} type 
 * @property {string} status 
 * @property {object} payload 
 * @property {string} DbType 
 */

module.exports = {
  /**
   * Query tasks from database.
   * @param {TasksFilter} filter 
   */
  async query(filter) {
    //todo: validate filter
    let res = await db.tasks.query(filter);
    return res;
  }, 

/**
 * Creates task from notification payload
 * @argument {object} notification
 * @returns {TaskModel}
 */
  async createFromNotification(notification){
    let task = {
      id: notification.id,
      type: 'push-notification.broadcast',
      category: 'system',
      payload: notification,
      result: null,
      validFromTimestamp: null,
      validToTimestamp: null,
      createdTimestamp: Date.now(),
      completedTimestamp: null,
      modifiedTimestamp: null,
      failures: null,
      lockTimestamp: null,
      Created: new Date().toISOString(),
      Modified: null,
      DbType: 'task'
    };
    return await db.tasks.upsert(task);
  }
}
