const PushNotifications = require('node-pushnotifications');
const config = require('../config');
const errors = require('../errors');
const log = require('../logger');
const devices = require('./devices');

const settings = {
  gcm: {
    id: config.android.key,
    phonegap: true //https://github.com/phonegap/phonegap-plugin-push/blob/master/docs/PAYLOAD.md#android-behaviour
  },
  apn: config.ios
}
const push = new PushNotifications(settings);
/**
 * @typedef NotificationDto api input model
 * @property {boolean} whatif If set to true, service does not send notification.
 * @property {boolean} broadcast If set to true, notification will be sent to all users.
 * @property {string[]} to Array of recipients, deviceTokens or emails.
 * @property {object} msg Push notification content.
 */

module.exports = { 
  send,
  notifySystemAccounts
};

//todo: come up with consistent response format both for simulation (whatif) and real push. 

/**
 * Sends push notification
 * @param {NotificationDto} dto 
 * @returns {Promise<object>}
 */
async function send(dto){
  if(!dto.broadcast && !Array.isArray(dto.to)) errors.validationError('notification.to is exptected to be array of recipients');
  if(!dto.msg) errors.validationError('notification.msg is not supplied');
  
  let msg = dto.msg;
  let tokens = [];
  if(dto.broadcast === true){
    //todo: create task, return taskId
    errors.validationError('broadcasting is temporarily disabled');
  }
  if(dto.strict){
    tokens = dto.to;
  } else {
    log.debug({msg: `fetching devices from db`, data: dto.to });
    let platform = dto.platform || dto.msg.platform;
    tokens = await devices.deviceTokens(dto.to, { platform });
  }

  if(dto.whatif){
    log.debug({msg: 'notification will no be sent due to whatif flag' });
    return tokens;
  }
  
  log.debug({msg: `notification will be sent to ${tokens.length} devices` });

  msg.topic = config.ios.topic; //ensure 
  let response = await push.send(tokens, msg);
  return fixApnResponse(response);
}

/**
 * Send notification to system accounts
 * @param {object} msg notification
 * @param {string} msg.title notification title
 * @param {string} msg.body notification message
 */
async function notifySystemAccounts(msg){
  const notification = {
    title: 'System notification',
    body: 'Created by notification service',
    sound: 'default',
    subtitle: 'subtitle',
    icon: 'ic_launcher',
    custom: {
      type: "system",
    }
  };
  Object.assign(notification, msg);
  return await send({ to: config.systemAccounts, msg: notification });
}
/**
 * node-pushnotifications: https://github.com/appfeel/node-pushnotifications
 * gcm: https://developers.google.com/cloud-messaging/http-server-ref#table5
 * fcm: https://firebase.google.com/docs/cloud-messaging/server
 * apn: https://developer.apple.com/library/content/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/CommunicatingwithAPNs.html#//apple_ref/doc/uid/TP40008194-CH11-SW1
 */

const responseExample = [{ method: "apn", message: [
  { "regId": {"device": "0a3ca20c67fef648ca9555f2f879deee2f212ce803bd72c9eac04207f1e7350f"}, "error": null }, // this one should be fixed
  { "regId": "0beffdfe32e880bbd150d0aa020d3884defbb07ceb051de8c0bacd4d3ae19c0c", "error": { "message": "Unregistered" }}
]}];

 /**
  * (1) issue with apn notification response formatting:
  * node-pushnotifications 1.0.18 expects "apn": "^2.1.1", however with "apn": "2.1.5" incorrectly misinterprets the results
  * (sendAPN.js line 47) resulting regId being object and string in the same response array
  */
// todo: incpect code changes in from "apn": "2.1.1" to "apn": "2.1.5", if nothing changed in response format, submit fix & ticket to node-pushnotifications
function fixApnResponse(response){
  let apn = responseByMethod(response, 'apn');
  if(apn != null){
    mutateToUnifirmRegId(apn);
  }
  return response;
}

function responseByMethod(response, methodName){
  if(response == null || !Array.isArray(response) || typeof methodName !== 'string'){
    return null;
  }
  for(let methodResponse of response){
    if(methodResponse != null && methodResponse.method === methodName){
      return methodResponse;
    }
  }
  return null;
}

function mutateToUnifirmRegId(result){
  if(!Array.isArray(result.message)){
    return result;
  }
  for(let deviceResult of result.message){
    if(deviceResult != null && deviceResult.regId != null && typeof deviceResult.regId === 'object' && typeof deviceResult.regId.device === 'string'){
      deviceResult.regId = deviceResult.regId.device;
    }
  }
  return result;
}