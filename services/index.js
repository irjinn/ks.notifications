module.exports = {
  sender: require('./sender'),
  tasks: require('./tasks'),
  devices: require('./devices')
}