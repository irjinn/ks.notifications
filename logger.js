require('./utils/serialize-errors')()
const loggly = require('node-loggly-bulk');
const config = require('./config')
/**
 * @typedef Logger
 * @property {Function} trace
 * @property {Function} debug
 * @property {Function} info
 * @property {Function} warn
 * @property {Function} error
 */

const useLoggly = config.loggly.customerToken && config.loggly.subdomain;
const client = useLoggly
  ? loggly.createClient({
    token: config.loggly.customerToken,
    subdomain: config.loggly.subdomain,
    tags: config.loggly.tags,
    json: true,
  })
  : {};

const logLevel = {
  trace: 'trace',
  debug: 'debug',
  info: 'info',
  warn: 'warn',
  error: 'error'
};
const logLevelCodes = {
  trace: 10,
  debug: 20,
  info: 30,
  warn: 40,
  error: 50
};
const minLogglyLevelCode = logLevelCodes[config.loggly.minLevel] || 30;
function shouldWriteToLoggly(level){
  let code = logLevelCodes[level] || logLevelCodes.debug;
  return useLoggly && (code >= minLogglyLevelCode);
}

module.exports = {
  level: logLevel,
  log: writeAndForget,
  trace: msg => writeAndForget(logLevel.trace, msg),
  debug: msg => writeAndForget(logLevel.debug, msg),
  info: msg => writeAndForget(logLevel.info, msg),
  warn: msg => writeAndForget(logLevel.warn, msg),
  error: msg => writeAndForget(logLevel.error, msg),
  logAsync: (level, msg) => write(level, msg)
}

function composeLogMessage(level, msg){
  if(typeof msg === 'string'){
    msg = { msg };
  }
  msg.appName = config.appName;
  msg.environment = config.environment;
  msg.level = level || logLevel.info;
  return msg;
}

function writeAndForget(level, msg){
  console.dir(msg);
  if(!shouldWriteToLoggly(level)){
    return;
  }
  msg = composeLogMessage(level, msg)
  client.log(msg);
}

function write(level, msg){
  console.dir(msg);
  if(!shouldWriteToLoggly(level)){
    return Promise.resolve({});
  }
  msg = composeLogMessage(level, msg);
  return new Promise((resolve, reject) => {
    client.log(msg, (err, res) => {
      if(err) reject(err);
      if(res) resolve(res);
    });
  })
}
