module.exports = {
  /**
   * Throw validation error that indicates bad request.
   * @param {string} msg error message
   * @param {object} data optional data that will be serialized with exception 
   */
  validationError(msg, data){
    let err = new Error();
    err.status = 400;
    err.message = msg;
    if(data){
      err.data = data;
    }
    throw err;  
  }
}