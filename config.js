require('./utils/extensions');
const fs = require('fs');
const appName = require('./package.json').name;
let localConfig = {
  api: {},
  documentdb: {
    collections: {},
  },
  systemAccounts: null,
  android: {},
  ios: {},
  loggly: {}
};

if(fs.existsSync('config.local.js')){
  localConfig = require('./config.local.js');
  console.log(`using local configuration from config.local.js`);
}

module.exports = {
  environment: localConfig.environment || process.env['environment'] || 'Develop',
  appName: appName,
  api: {
    port: process.env.PORT || 3000,
    key: process.env['auth:apikey'] || '6d1b9cfc-c6da-4680-838e-77f90f9c2857'
  },
  documentdb: {
    endpoint: localConfig.documentdb.endpoint || process.env['documentdb:endpoint'],
    key: localConfig.documentdb.key || process.env['documentdb:key'],
    collections: {
      devices: localConfig.documentdb.collections.devices || process.env['documentdb:collections:devices'],
      tasks: localConfig.documentdb.collections.tasks || process.env['documentdb:collections:tasks']
    }
  },
  systemAccounts: (localConfig.systemAccounts || process.env['systemAccounts'] || 'irjinn@gmail.com').split(','),
  android: {
    key: localConfig.android.key || process.env['android:key']
  },
  ios: {
    cert: localConfig.ios.cert || getMultilineString(process.env['ios:cert']),
    key: localConfig.ios.key || getMultilineString(process.env['ios:key']),
    topic: localConfig.ios.topic || 'com.kitabsawti.kitabsawti',
    production: localConfig.ios.production || (process.env['ios:production'] === 'true')
  },
  loggly: {
    customerToken: localConfig.loggly.customerToken || process.env['loggly:customerToken'],
    subdomain: localConfig.loggly.subdomain || process.env['loggly:subdomain'],
    tags: [appName],
    minLevel: process.env['loggly:level'] || 'info'
  }
};

function getMultilineString(value){
  if(value == null || typeof value !== 'string') return value;
  return value.replaceAll('\\\\n', '\n').replaceAll('\\n', '\n'); 
}