#!/bin/bash
env=test 
file="book"
# url="https://$env-ks-notifications.azurewebsites.net/push"
url="http://localhost:3000/push"
# apiKey="b900d403-c255-496f-a44a-24288622614b"
apiKey="6d1b9cfc-c6da-4680-838e-77f90f9c2857" # local
# override variables using command line arguments
# usage: bash send-notification.sh -file "info-message"
until [ $# -eq 0 ]
do
  name=${1:1}; shift;
  if [[ -z "$1" || $1 == -* ]] ; then eval "export $name=true"; else eval "export $name=$1"; shift; fi  
done
echo "using $file.json as a request body"
curl -X POST \
  -H "Authorization: $apiKey" \
  -H "Content-Type: application/json" \
  -d @"$file.json" \
  "$url"