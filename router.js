const fs = require('fs');
const Router = require('koa-router');
const sender = require('./services').sender;
const devices = require('./services').devices;
const tasks = require('./services').tasks;
const config = require('./config');
// const db = require('./db');
const log = require('./logger');
const errors = require('./errors');

const router = module.exports = new Router(); // router.get|put|post|patch|delete|del 

router.get('/', function (ctx, next) {
  ctx.response.type = 'text/html';
  ctx.response.body = 'Notifications';
});

router.get('/devices', async function (ctx, next) {
  ctx.ensureAuthenticated();
  ctx.response.body = await devices.query(ctx.query);
});

router.post('/devices/query', async function (ctx, next) {
  ctx.ensureAuthenticated();
  ctx.response.body = await devices.query(ctx.request.body);
});

router.post('/echo', function (ctx, next) {
  ctx.ensureAuthenticated();
  ctx.response.body = ctx.request.body;
});

/**
 * Send push notification to system accounts 
 */
router.post('/push/system', async function (ctx, next) {
  ctx.ensureAuthenticated();
  let data = ctx.request.body;
  if(!data) errors.validationError('request body is empty');
  if(!data.title) errors.validationError('message.title is not set');
  if(!data.body) errors.validationError('message.body is not set');

  let res = await sender.notifySystemAccounts(data); 
  ctx.response.body = res;
});

/**
 * Send notification to set of users
 */
router.post('/push', async function(ctx, next){
  ctx.ensureAuthenticated();
  let data = ctx.request.body;
  if(!data) errors.validationError('request body is empty');

  let response = await sender.send(data);
  ctx.response.type = 'json';
  ctx.response.body = response;
});

/**
 * Schedule broadcast to all users 
 */
router.post('/push/broadcast', async function(ctx, next){
  ctx.ensureAuthenticated();
  let data = ctx.request.body;
  if(!data) errors.validationError('request body is empty');

  //todo: validate push notification
  let task = await tasks.createFromNotification(data);
  await sender.notifySystemAccounts({ title: 'Broadcast scheduled', body: new Date().toISOString() });
  ctx.response.type = 'json';
  ctx.response.body = task;
});

/**
 * koa: http://koajs.com/
 * koa-router: https://github.com/alexmingoia/koa-router
 * koa-examples: https://github.com/koajs/examples
 */