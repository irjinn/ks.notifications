require('./utils/extensions');
const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const app = new Koa();
const config = require('./config');

const log = require('./logger');
const middleware = require('./middleware');
const router = require('./router');
const port = config.api.port; 

app
  .use(middleware.cors)
  .use(middleware.serverError)
  .use(middleware.logger)
  .use(middleware.auth)
  .use(middleware.badRequest)
  .use(bodyParser())
  .use(router.routes())
  .use(router.allowedMethods())
  .use(middleware.notFound);

app.listen(port);
log.debug(`app is listening on port ${port}`);

process.on('uncaughtException', err => log.error({err: err, msg: 'uncaught exception' }));
process.on('unhandledRejection', err => log.error({err: err, msg: 'unhandled rejection' }));
